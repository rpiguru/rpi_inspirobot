import os

# Output to HDMI
os.environ['KIVY_BCM_DISPMANX_ID'] = '2'


import glob
import random
from kivy.animation import Animation
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.config import Config
from kivy.config import _is_rpi

from utils import inspirobotSays0

# Set resolution
Config.read(os.path.expanduser('~/.kivy/config.ini'))
Config.set('graphics', 'width', '640')
Config.set('graphics', 'height', '480')


Builder.load_file(os.path.join(os.path.dirname(__file__), 'main.kv'))


BUTTON_PIN = 21      # GPIO 21

if _is_rpi:
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)


class MainWidget(FloatLayout):

    def on_touch_down(self, touch):
        super(MainWidget, self).on_touch_down(touch)
        if not _is_rpi:
            App.get_running_app().on_btn_pressed()


class InspirobotApp(App):

    root = None
    img = None
    step = 'boot'
    is_running = False

    def build(self):
        self.root = MainWidget()
        self.img = Image(source='posters/welcome/boot.jpg')
        self.root.add_widget(self.img)
        return self.root

    def on_btn_pressed(self, *args):
        if not self.is_running:
            if self.step == 'boot':
                self.img.source = 'posters/welcome/welcome.jpg'
                self.step = 'processing'
            else:
                self.start_process()

    def start_process(self):
        self.is_running = True
        self.root.ids.title.text = random.choice(inspirobotSays0)
        self.root.remove_widget(self.img)
        self.img = None
        anim = Animation(opacity=1, duration=1)
        anim.bind(on_complete=self.show_picture)
        anim.start(self.root.ids.img_light)

    def show_picture(self, *args):
        self.root.ids.img_light.opacity = 0
        self.img = Image(source=random.choice(glob.glob('posters/*.jpg')),
                         size_hint=(None, None), size=(10, 10),
                         pos_hint={'center_x': .5, 'center_y': .5})
        self.root.add_widget(self.img)
        anim = Animation(size=(640, 480), duration=.5, t='in_out_cubic')
        anim.bind(on_complete=self.get_ready)
        anim.start(self.img)

    def get_ready(self, *args):
        self.is_running = False


if __name__ == '__main__':
    app = InspirobotApp()
    if _is_rpi:
        GPIO.add_event_detect(BUTTON_PIN, GPIO.FALLING, callback=app.on_btn_pressed, bouncetime=200)

    app.run()
