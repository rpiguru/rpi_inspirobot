#!/usr/bin/env bash

wget https://github.com/mozilla/geckodriver/releases/download/v0.18.0/geckodriver-v0.18.0-arm7hf.tar.gz
tar xzvf geckodriver-v0.18.0-arm7hf.tar.gz
rm geckodriver-v0.18.0-arm7hf.tar.gz
sudo chmod +x geckodriver
sudo cp geckodriver /usr/local/bin/

sudo apt-get update
sudo apt-get install -y firefox-esr xvfb
sudo pip install selenium python-uinput
sudo pip install xvfbwrapper
sudo echo "uinput" | sudo tee -a /etc/modules
sudo modprobe uinput

cur_dir="$( cd "$(dirname "$0")" ; pwd -P )"
echo "@sudo python ${cur_dir}/main_selenium.py" | tee -a /home/pi/.config/lxsession/LXDE-pi/autostart
