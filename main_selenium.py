#!/usr/bin/env python

import os
import time
from selenium import webdriver
import uinput
from utils import is_rpi


BUTTON_PIN = 21      # GPIO 21


if is_rpi():
    import RPi.GPIO as GPIO
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)


# os.environ["DISPLAY"] = ":0.0"
os.system('sh -c "TERM=linux setterm -blank 0 >/dev/tty0"')

cur_dir = os.path.dirname(os.path.realpath(__file__))
html_file = 'file://{}/index.html'.format(cur_dir)

device = uinput.Device([uinput.KEY_F11])

browser = webdriver.Firefox()
browser.get(html_file)
device.emit_click(uinput.KEY_F11)


def on_btn_pressed(*args):
    print('Button is pressed! Replacing image...')
    browser.execute_script('generateImage();')


if is_rpi():
    GPIO.add_event_detect(BUTTON_PIN, GPIO.FALLING, callback=on_btn_pressed, bouncetime=200)

while True:
    time.sleep(1)
